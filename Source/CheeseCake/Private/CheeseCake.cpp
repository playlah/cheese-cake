// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "CheeseCake.h"

#define LOCTEXT_NAMESPACE "FCheeseCakeModule"

void FCheeseCakeModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	UE_LOG(LogTemp, Warning, TEXT("CheeseCake Module loaded."));
}

void FCheeseCakeModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FCheeseCakeModule, CheeseCake)