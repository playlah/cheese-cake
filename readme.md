# Cheese Cake

Cheese Cake is an unreal framework for game artisans.

## Software Requirement

- [Unreal Engine 4](https://www.unrealengine.com/en-US/blog)
- [Visual C++ Redistributable 2015](https://www.microsoft.com/en-us/download/details.aspx?id=48145)
- [Visual C++ Redistributable 2013](https://www.microsoft.com/en-us/download/details.aspx?id=40784)
- [Visual C++ Redistributable 2012](https://www.microsoft.com/en-us/download/details.aspx?id=30679)
- [Visual C++ Redistributable 2010 x64](https://www.microsoft.com/en-US/Download/confirmation.aspx?id=14632)
- [Visual C++ Redistributable 2010 x86](https://www.microsoft.com/en-us/download/details.aspx?id=5555)
- [Visual C++ Redistributable 2008](https://www.microsoft.com/en-us/download/details.aspx?id=11895)
- [Visual C++ Redistributable 2005 x64](https://www.microsoft.com/en-US/Download/confirmation.aspx?id=21254)
- [Visual C++ Redistributable 2005 x86](https://www.microsoft.com/en-us/download/details.aspx?id=3387)
- [Visual Studio 2015 or higher](https://www.visualstudio.com/downloads/)
- [Git LFS](https://git-lfs.github.com/)

### For Windows 8.1 User ONLY

The following packages must be installed in ascending order.

- [March 2014](https://support.microsoft.com/en-us/help/2919442/march-2014-servicing-stack-update-for-windows-8-1-and-windows-server-2)
- [April 2014](https://support.microsoft.com/en-us/help/2919355/windows-rt-8-1--windows-8-1--and-windows-server-2012-r2-update-april-2)
- [Universal C Runtime](https://support.microsoft.com/en-us/help/2999226/update-for-universal-c-runtime-in-windows)

**IMPORTANT:** Unreal Engine must have these packages in order to run properly.

## Installation

Before going deeper, assumed that you have already setup your own game project.
If not, please setup a game project before you proceed further.

After installing the required softwares, place the current project into the following path **{GameProject}/Plugins**.
This is because the current project is considered as a **plugin** for Unreal Engine.

## Getting Started

To get the project up and running, open your game project.

Note: If you just want to compile the project without opening a new Unreal Editor, 
in menu bar, choose **Build** > **Build Solution (F7)**.


